"""
docstring
"""
TASK_MAX = 4096
NAME_LEN_MAX = 64
NODE_MAX = 8
MEM_MAX = 500000000
PER_CPU_MAX = 10000000

class Field:
    def __init__(
            self,
            command,
            name="",
            prompt="",
            desc="",
            value=None,
            req=False,
            is_valid = None
            ):

        self.command = command
        self.name = name
        self.prompt = prompt
        self.desc = desc
        self.value = value
        self.is_valid = is_valid
        self.req = req

    def get(self):
        pass

fields = [
    Field(
        command = "--job-name=",
        name = "job name",
        prompt = "",
        desc = "Gives your job a name, useful when examining the queue",
        value = "",
        req = True,
        is_valid = (lambda x: x != "" and len(x) < NAME_LEN_MAX)
    ),
    Field(
        command = "--ntasks=",
        name = "number of tasks",
        prompt = "",
        desc = "Sets the requested CPUs for the job",
        value = "",
        req = False,
        is_valid = (lambda x: int(x) > 0 and int(x) < TASK_MAX)
    ),
    Field(
        command = "--nodes=",
        name = "number of nodes",
        prompt = "",
        desc = "Sets the number of nodes you wish to use",
        value = "",
        req = False,
        is_valid = (lambda x: int(x) > 0 and int(x) < NODE_MAX)
    ),
    Field(
        command = "--time=",
        name = "allowed time",
        prompt = "D-HH:MM:SS (D- can be omitted)",
        desc = "Sets the allowed run time for the job",
        value = "",
        req = False
    ),
    Field(
        command = "--mail-type=",
        name = "time to mail",
        prompt = "",
        desc = "Sets when you would like the scheduler to notify you about a job",
        value = "",
        req = False
    ),
    Field(
        command = "--mail-user=",
        name = "email address",
        prompt = "x@umsystem.edu",
        desc = "Sets the mailto address for this job",
        value = "",
        req = False
    ),
    Field(
        command = "--export=",
        name = "export variables",
        prompt = "",
        desc = """Slurm exports the current environment variables so all
        loaded modules will be passed to the environment of the job""",
        value = "",
        req = False
    ),
    Field(
        command = "--mem=",
        name = "memory amount",
        prompt = "",
        desc = "Number in MB of memory you would like the job to have access to",
        value = "",
        req = False,
        is_valid = (lambda x: int(x) > 0 and int(x) < MEM_MAX)
    ),
    Field(
        command = "--mem-per-cpu=",
        name = "memory amount per cpu",
        prompt = "",
        desc = """Number in MB of memory you want per cpu, default values vary
        by queue but are typically greater than 1000Mb""",
        value = "",
        req = False,
        is_valid = (lambda x: x.isdigit() and int(x) > 0 and int(x) < PER_CPU_MAX)
    ),
    Field(
        command = "--nice=",
        name = "job priority",
        prompt = "",
        desc = "The higher the nice number the lower the priority",
        value = "",
        req = False
    ),
    Field(
        command = "--constraint=",
        name = "constraints",
        prompt = "",
        desc = "intel, amd, EDR, FDR, QDR, DDR, serial, gpu, cpucodename",
        value = "",
        req = False
    ),
    Field(
        command = "--gres=",
        name = "reserved resources",
        prompt = "",
        desc = """Allows the user to reserve additional resources on the node,
        specifically for our cluster gpus. e.g. –gres=gpu:2 will
        reserve 2 gpus on a gpu enabled node""",
        value = "",
        req = False
    ),
    Field(
        command = "--p ",
        name = "partition",
        prompt = "",
        desc =
        """If not defined jobs get routed to the highest priority partition 
        your user has permission to use""",
        value = "",
        req = False
    ),
]

"""
Print all info for a field, no keywords.

get input ->

blank noreq -> pass
valid req/noreq -> pass

blank req -> try again
invalid req -> try again
invalid noreq -> try again
"""

def get_input(s_field):
    """Takes in a field and gets required user input for it"""
    if s_field.req:
        if (
            (s_field.value == "") or
            (not s_field.is_valid(s_field.value))
            ):
            print("sorry that's no good.")
            get_input(s_field)
    else:
        if not s_field.is_valid(s_field.value):
            print("sorry that's no good")
            get_input(s_field)

for field in fields:
    field.value = input("please enter the " + field.name +
                    " or press enter to skip.\n" +
                    field.prompt)
    if field.value.lower() == "help" :
        print(field.desc)

with open("jobfile.sub", "w+", encoding="t") as jfile:
    for field in fields:
        if field.value != "" :
            jfile.write("#SBATCH " + field.command + field.value)
